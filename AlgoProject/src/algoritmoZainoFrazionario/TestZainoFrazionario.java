package algoritmoZainoFrazionario;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestZainoFrazionario {

	ZainoFrazionario zaino;
	
	@Test
	public void testMaxVal() {
		double cap = 6;
		double[] valore = {5,1,6};
		double[] volume = {3,4,3};
		
		zaino = new ZainoFrazionario(cap,valore,volume);
		assertEquals(11,zaino.maxVal());
	}
	
	@Test
	public void testDose() {
		testMaxVal();
		assertEquals(0,zaino.dose(1),0.00001);
		assertEquals(1,zaino.dose(0),0.00001);
		assertEquals(1,zaino.dose(2),0.00001);
	}

}
