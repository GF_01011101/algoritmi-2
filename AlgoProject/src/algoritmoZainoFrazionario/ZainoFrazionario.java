package algoritmoZainoFrazionario;

import it.uniupo.algoTools.*;


public class ZainoFrazionario {
	double capacità;
	double[] dose;
	double[] valore;
	double[] volume;
	
	public ZainoFrazionario(double capacità, double[] valore, double[] volume) {
		this.capacità = capacità;
		this.valore = valore;
		this.volume = volume;
		dose = new double[volume.length];
	}
	
	
	private int zainoFraz() {
		MaxHeap<Integer,Double> heap = new MaxHeap<Integer,Double>();
		double[] quantità = new double[volume.length];
		for(int i=0;i<volume.length;i++) {
			heap.add(i, (valore[i]/volume[i]));
			dose[i] = 0;
			quantità[i] = 0;
		}
		int valTotale = 0;
		double spazioRimasto = capacità;
		while(spazioRimasto>0 && !heap.isEmpty()) {
			Integer materiale = heap.extractMax();
			if(volume[materiale]<=spazioRimasto) {
				dose[materiale] = 1;
				quantità[materiale] = volume[materiale]*dose[materiale];
				valTotale += valore[materiale];
				spazioRimasto -= volume[materiale];
			}
			else {
				dose[materiale] = spazioRimasto/volume[materiale];
				quantità[materiale] = spazioRimasto;
				valTotale += valore[materiale] * dose[materiale];
				spazioRimasto = 0;
			}
		}
		return valTotale;
	}
	
	public int maxVal() {
		return zainoFraz();
	}
	
	public double dose(int i) {
		zainoFraz();
		return dose[i];
	}
}
