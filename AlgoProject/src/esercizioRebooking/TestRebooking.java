package esercizioRebooking;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestRebooking {

	@Test
	public void test() {
		Rebooking test = new Rebooking(10);
		int[] dimGruppo = {8,2,10};
		int[] costoGruppo = {8,3,10};
		assertEquals(11,test.maxSaving(dimGruppo, costoGruppo));
	}
	
	@Test
	public void test2() {
		Rebooking test = new Rebooking(-3);
		int[] dimGruppo = {8,2,10};
		int[] costoGruppo = {8,3,10};
		assertEquals(-1,test.maxSaving(dimGruppo, costoGruppo));
	
	}
	
	@Test
	public void test3() {
		Rebooking test = new Rebooking(10);
		int[] dimGruppo = {8,2,10,1};
		int[] costoGruppo = {8,3,10};
		assertEquals(-1,test.maxSaving(dimGruppo, costoGruppo));
	}
	
	@Test
	public void test4() {
		Rebooking test = new Rebooking(10);
		int[] dimGruppo = {8,1,10};
		int[] costoGruppo = {8,3,10};
		assertEquals(11,test.maxSaving(dimGruppo, costoGruppo));
	}
	
	@Test
	public void test5() {
		Rebooking test = new Rebooking(10);
		int[] dimGruppo = {8,2,10};
		int[] costoGruppo = {8,3,10,3};
		assertEquals(-1,test.maxSaving(dimGruppo, costoGruppo));
	}
	
	@Test
	public void test6() {
		Rebooking test = new Rebooking(10);
		int[] dimGruppo = {8,2,10};
		int[] costoGruppo = {8,2,10};
		assertEquals(10,test.maxSaving(dimGruppo, costoGruppo));
	}
	
	@Test
	public void test7() {
		Rebooking test = new Rebooking(10);
		int[] dimGruppo = {8,2,10};
		int[] costoGruppo = {8,1,10};
		assertEquals(10,test.maxSaving(dimGruppo, costoGruppo));
	}

	@Test
	public void test8() {
		Rebooking test = new Rebooking(10);
		int[] dimGruppo = {5,8,3};
		int[] costoGruppo = {3,11,9};
		assertEquals(12,test.maxSaving(dimGruppo, costoGruppo));
		Rebooking test2 = new Rebooking(-4);
		int[] dimGruppo2 = {5,8,3};
		int[] costoGruppo2 = {3,11,9};
		assertEquals(-1,test2.maxSaving(dimGruppo2, costoGruppo2));
		Rebooking test3 = new Rebooking(10);
		int[] dimGruppo3 = {5,8};
		int[] costoGruppo3 = {3,11,9};
		assertEquals(-1,test3.maxSaving(dimGruppo3, costoGruppo3));
		Rebooking test4 = new Rebooking(0);
		int[] dimGruppo4 = {5,8,3};
		int[] costoGruppo4 = {3,11,9};
		assertEquals(0,test4.maxSaving(dimGruppo4, costoGruppo4));
		Rebooking test5 = new Rebooking(1);
		int[] dimGruppo5 = {5,8,3};
		int[] costoGruppo5 = {3,11,9};
		assertEquals(0,test5.maxSaving(dimGruppo5, costoGruppo5));
		Rebooking test6 = new Rebooking(20);
		int[] dimGruppo6 = {5,8,3};
		int[] costoGruppo6 = {3,11,9};
		assertEquals(23,test6.maxSaving(dimGruppo6, costoGruppo6));
		
	}
}
