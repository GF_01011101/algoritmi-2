package esercizioRebooking;

public class Rebooking {
	
	int cap;
	int[][] val;
	
	public Rebooking(int availableSeats) {
		this.cap = availableSeats;
	}
	
	public int maxSaving(int[] groupSize, int[] groupCosts) {
		if(cap<0 || groupSize.length!=groupCosts.length) return -1;
		val = new int[groupSize.length+1][cap+1];
		for(int i=0;i<=cap;i++) {
			val[0][i] = 0;
		}
		for(int h=1;h<=groupSize.length;h++) {
			for(int k=0;k<=cap;k++) {
				if(groupSize[h-1]<=k) {
					val[h][k] = Math.max(val[h-1][k], val[h-1][k-groupSize[h-1]]+groupCosts[h-1]);
				}
				else {
					val[h][k] = val[h-1][k];
				}
			}
		}
		return val[groupSize.length][cap];
	}
}
