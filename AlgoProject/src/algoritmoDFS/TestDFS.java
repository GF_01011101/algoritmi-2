package algoritmoDFS;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;


import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

/*********
 * le seguenti tre righe servono per eseguire i test in ordine
 * Non e' corretto perche' i test devono avere successo in qualunque 
 * ordine siano eseguiti, ma serve per il lavoro in lab:
 * lavorate in modo che i test abbiano successo dal primo in avanti
 */
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TestDFS {
    GraphInterface testGraph;
    DFS dfsTest;
    
    //********************************
    //testCreate(): non ci sono errori nel costruttore
    
    @Test
    public void test00Create() {
        testGraph = new UndirectedGraph(1);
        dfsTest = new DFS(testGraph);
        assertNotNull(dfsTest);
    }
    
     
    //********test base***************
    
    //deve funzionare correttamente in un grafo banale con un solo nodo e senza archi
    @Test
    public void test10OneNodeVisited() { //DFS su grafo con un solo nodo
        testGraph = new UndirectedGraph(1);
        dfsTest = new DFS(testGraph);
        GraphInterface tree = dfsTest.getDFStree(0);
        int edges = tree.getEdgeNum();
        assertEquals("Non ci sono archi", 0, edges);
    }

    //deve funzionare correttamente in un grafo banale con due nodi e un arco
    @Test
    public void test11TwoNodesVisited() { //DFS su grafo con due nodi
        testGraph = new UndirectedGraph(2);
        testGraph.addEdge(1, 0);
        dfsTest = new DFS(testGraph);
        GraphInterface tree = dfsTest.getDFStree(0);
        int edges = tree.getEdgeNum();
        assertEquals("C'e' un arco", 1, edges);

    }
    
    //************verifica che la visita sia realmente una DFS***********
   @Test
   public void test15DFSOrder() {
	   testGraph = new UndirectedGraph("4;0 2;0 1;2 3;1 3");
       dfsTest = new DFS(testGraph);
       GraphInterface tree = dfsTest.getDFStree(2);
       assertTrue("L'albero di visita in profondita' con sorgente 2 deve avere l'arco (2,3) o lo (0,2) ma non entrambi",!(tree.hasEdge(2,3))  ||
    		   !(tree.hasEdge(0,2)) );
       assertTrue("L'albero di visita in profondita' con sorgente 2 deve avere gli archi (1,3) e (0,1)",(tree.hasEdge(1,3))  &&
    		   (tree.hasEdge(0,1)));
       
   }
   
   @Test
   public void test17DFSOrder() {
	   testGraph = new UndirectedGraph("4;1 0;0 2;3 2;3 1");
	   dfsTest = new DFS(testGraph);
	   ArrayList<Integer> nodiVisitati = dfsTest.getNodesInOrderOfVisit(1);
	   assertTrue("Il primo nodo visitato è il 3",nodiVisitati.get(0)==3);
	   
   }
   
   @Test
   public void test19DFSForest() {
	   testGraph = new UndirectedGraph("5;0 2;0 1;2 1;4 3");
       dfsTest = new DFS(testGraph);
       GraphInterface tree = dfsTest.getDFStree(4);
       assertTrue("L'albero di visita in profondita' con sorgente 4 deve avere l'arco (4,3)"
       		+ " e nessun altro arco perché non un grafo connesso",(tree.hasEdge(4,3)) && !(tree.hasEdge(0,1)) && !(tree.hasEdge(0,2)) && !(tree.hasEdge(2,1)));
       dfsTest = new DFS(testGraph);
       GraphInterface forest = dfsTest.getForest();
       assertTrue(forest.hasEdge(0,2) && forest.hasEdge(2,1) && forest.hasEdge(4,3) && !forest.hasEdge(2,3));
       assertTrue(!forest.hasEdge(2,4));
       assertTrue(!forest.hasEdge(4,0));
       assertTrue(!forest.hasEdge(1,4));
   }
   
   //*************test inizializzazione******************
   
   //numero di archi
   @Test 
   public void test20InitArchi() {
     GraphInterface grafo = 
   		new UndirectedGraph ("4;0 2;0 1;2 3;1 3");
     dfsTest = new DFS(grafo); //<<- creato una volta sola
     assertTrue("L'albero di visita in profondita' con sorgente 2 deve avere l'arco (2,3) o lo (0,2) ma non entrambi",!(dfsTest.getDFStree(2).hasEdge(2,3))  ||
  		   !(dfsTest.getDFStree(2).hasEdge(0,2)) );
     assertTrue("L'albero di visita in profondita' con sorgente 0 deve avere l'arco (0,2) o lo (0,1) ma non entrambi",!(dfsTest.getDFStree(0).hasEdge(0,1))  ||
    		   !(dfsTest.getDFStree(0).hasEdge(0,2)) );
   }
   
  
  //***************test eccezione input metodo*************
   
   
   @Test (expected = IllegalArgumentException.class)
   public void test30illegalargBig(){
	   GraphInterface grafo = 
		   		new UndirectedGraph ("4;0 2;0 1;2 3;1 3");
		     DFS visit = new DFS(grafo);
		     visit.getDFStree(5);  
   }
   
   @Test (expected = IllegalArgumentException.class)
   public void test35illegalargSmall() {
	   GraphInterface grafo = 
		   		new UndirectedGraph ("4;0 2;0 1;2 3;1 3");
		     DFS visit = new DFS(grafo);
		     visit.getDFStree(-1);  
   }
   
   @Test (expected = IllegalArgumentException.class)
   public void test40illegalargOrd() {
	   GraphInterface grafo = 
		   		new UndirectedGraph ("4;0 2;0 1;2 3;1 3");
		     DFS visit = new DFS(grafo);
		     visit.getDFStree(4);  
   }
   
   @Test 
   public void test50illegalargLegal() {
	   GraphInterface grafo = 
		   		new UndirectedGraph ("4;0 2;0 1;2 3;1 3");
		     DFS visit = new DFS(grafo);
		     try {
		     visit.getDFStree(0);  
		     } catch (IllegalArgumentException e) {
		    	 fail("0 e' un argomento legittimo");
		     }
   }
   
   @Test
   public void test53OrdinamentoTopologico() {
	   GraphInterface grafo = new DirectedGraph("6;0 1;2 1;3 2;3 5;4 2");
	   DFS visit = new DFS(grafo);
	   for(int i=0;i<grafo.getOrder();i++) {
		   System.out.print(visit.getOrdinamentoTopologico()[i]+" ");
		 
	   }
	   System.out.println("\n"+visit.nodiInOrdineDiVisita);
	   
   }
   
    @Test
    public void test55hasDirCycle() {
    	 GraphInterface grafo = new DirectedGraph ("1");
 		 DFS visit = new DFS(grafo);
 		 assertTrue(!visit.hasDirCycle());
 		 GraphInterface grafo2 = new DirectedGraph ("2;0 1");
		 DFS visit2 = new DFS(grafo2);
		 assertTrue(!visit2.hasDirCycle());
		 GraphInterface grafo3 = new DirectedGraph ("3;1 0;1 2;0 2");
		 DFS visit3 = new DFS(grafo3);
		 assertTrue(!visit3.hasDirCycle());
		 GraphInterface grafo4 = new DirectedGraph ("3;0 2;2 1;1 0");
		 DFS visit4 = new DFS(grafo4);
		 assertTrue(visit4.hasDirCycle());
		 GraphInterface grafo5 = new DirectedGraph ("5;4 0;4 1;4 2;2 3;3 4");
		 DFS visit5 = new DFS(grafo5);
		 assertTrue(visit5.hasDirCycle());
    }
    
    @Test
    public void test60getDirCycle() {
    	 GraphInterface grafo = new DirectedGraph ("1");
		 DFS visit = new DFS(grafo);
		 assertTrue(visit.getDirCycle()==null);
		 GraphInterface grafo2 = new DirectedGraph ("2;0 1");
		 DFS visit2 = new DFS(grafo2);
		 assertTrue(visit2.getDirCycle()==null);
		 GraphInterface grafo3 = new DirectedGraph ("3;1 0;1 2;0 2");
		 DFS visit3 = new DFS(grafo3);
		 assertTrue(visit3.getDirCycle()==null);
		 GraphInterface grafo4 = new DirectedGraph ("3;0 2;2 1;1 0");
		 DFS visit4 = new DFS(grafo4);
		 assertTrue(visit4.getDirCycle().size()==3);
		 GraphInterface grafo5 = new DirectedGraph ("5;4 0;4 1;4 2;2 3;3 4");
		 DFS visit5 = new DFS(grafo5);
		 assertTrue(visit5.getDirCycle().size()==3);
		 
    }
    
    @Test
    public void test70TopologicalOrder() {
    	DirectedGraph grafo = new DirectedGraph("4;3 1;3 0;1 2;1 0;0 2");
    	DFS visita = new DFS(grafo);
    	assertTrue(visita.topologicalOrder().get(0)==3);
    	System.out.println("Ordine topologico --> " + visita.ordineTopologico + "\nOrdine di Fine Visita --> " + visita.nodiInOrdineDiVisita);
    }
    
}
