package algoritmoDFS;


import java.util.ArrayList;

import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;

public class DFS {
	GraphInterface grafo;
	boolean[] scoperto;
	boolean[] terminato;
	int[] padre;
	boolean ciclo = false;
	GraphInterface alberoDFS;
	ArrayList<Integer> nodiInOrdineDiVisita;
	ArrayList<Integer> nodiCheFormanoCiclo;
	int ultimoVisitato;
	int nodoIndietro;
	int[] numerazione;
	int contatore;
	ArrayList<Integer> ordineTopologico;
	
	public DFS (GraphInterface grafoInput) {
		
		this.grafo = grafoInput;
		this.scoperto = new boolean[grafo.getOrder()];
		this.terminato = new boolean[grafo.getOrder()];
		this.padre = new int[grafo.getOrder()];
		this.numerazione = new int[grafo.getOrder()];
		this.nodiInOrdineDiVisita = new ArrayList<Integer>();
		this.nodiCheFormanoCiclo = new ArrayList<Integer>();
		this.ordineTopologico = new ArrayList<Integer>();
	}
	
	
	private void init() {
		for(int i=0;i<grafo.getOrder();i++) {
			scoperto[i] = false;
			terminato[i] = false;
			numerazione[i] = 0;
			padre[i] = -1;
		}
		alberoDFS = grafo.create();
		this.nodiInOrdineDiVisita.clear();
		this.nodiCheFormanoCiclo.clear();
		contatore = grafo.getOrder() - 1;
		ordineTopologico.clear();
		
	}
	
	
	private void visitaDFS(int sorgente) {
		if(sorgente>=grafo.getOrder() || sorgente<0) throw new IllegalArgumentException("Argomento fuori dal range");
		scoperto[sorgente] = true;
		for(Edge uv : grafo.getOutEdges(sorgente)) {
			int v = uv.getHead();
			if(!scoperto[v]) {
				alberoDFS.addEdge(uv);
				visitaDFS(v);
				padre[v] = sorgente;
			}
			else if(scoperto[v] && !terminato[v]) {
				ciclo = true;
				ultimoVisitato = sorgente;
				nodoIndietro = v;
			}
		}
		this.nodiInOrdineDiVisita.add(sorgente); // ordine di visita terminata
		terminato[sorgente] = true;
		numerazione[sorgente] = contatore;
		contatore--;
		ordineTopologico.add(0, sorgente); // ordine topologico (è l'ordine di fine visita inverso)
	}
	
	private ArrayList<Integer> costruttoreCiclo(){
		int inizio = ultimoVisitato;
		nodiCheFormanoCiclo.add(inizio);
		while(inizio != nodoIndietro) {
			inizio = padre[inizio];
			nodiCheFormanoCiclo.add(padre[inizio]);
		}
		return nodiCheFormanoCiclo;
	}
	
	public int[] getOrdinamentoTopologico() {
		visitaDFScompleta();
		return numerazione;
		
	}
	
	public ArrayList<Integer> topologicalOrder(){
		visitaDFScompleta();
		return ordineTopologico;
	}
	
	public ArrayList<Integer> getDirCycle(){
		init();
		for(int i=0;i<grafo.getOrder();i++) {
			if(!scoperto[i]) {
				visitaDFS(i);
			}
			if(ciclo) return costruttoreCiclo();
		}
		return null;
	}
	
	public GraphInterface getDFStree (int sorgente){
		init();
		visitaDFS(sorgente);
		return alberoDFS;
	}
	
	public ArrayList<Integer> getNodesInOrderOfVisit(int sorgente){
		init();
		visitaDFS(sorgente);
		return this.nodiInOrdineDiVisita; 
	 }
	
	private void visitaDFScompleta() {
		init();
		for(int i = 0;i<grafo.getOrder();i++) {
			if(!scoperto[i]) {
				visitaDFS(i);		
			}	
		}
	}
	
	public GraphInterface getForest() {
		visitaDFScompleta();
		return alberoDFS;
	}
	
	public boolean hasDirCycle() {
		visitaDFScompleta();
		return ciclo;
	}
	
}
