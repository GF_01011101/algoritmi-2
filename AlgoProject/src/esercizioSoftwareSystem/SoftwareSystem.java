package esercizioSoftwareSystem;

import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;

public class SoftwareSystem {
	
	DirectedGraph system;
	boolean[] scoperto;
	boolean[] terminato;
	boolean ciclo;
	
	public SoftwareSystem(DirectedGraph system) {
		this.system = system;
		this.scoperto = new boolean[system.getOrder()];
		this.terminato = new boolean[system.getOrder()];
	}
	
	private void init() {
		for(int i=0;i<system.getOrder();i++) {
			scoperto[i] = terminato[i] = false;
		}
		ciclo = false;
		
	}
	
	private void DFS(int sorgente) {
		scoperto[sorgente] = true;
		for(Edge uv : system.getOutEdges(sorgente)) {
			Integer v = uv.getHead();
			if(!scoperto[v]) {
				DFS(v);
			}
			else if(scoperto[v] && !terminato[v]) {
				ciclo = true;
			}
		}
		terminato[sorgente] = true;
	}
	
	private void DFSCompleta() {
		init();
		for(int i=0;i<system.getOrder();i++) {
			if(!scoperto[i]) DFS(i);
		}
	}
	
	public boolean hasCycle() {
		DFSCompleta();
		if(ciclo) return true;
		return false;
	}
}
