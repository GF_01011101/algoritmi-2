package esercizioSoftwareSystem;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniupo.graphLib.DirectedGraph;

public class TestSoftwareSystem {

	SoftwareSystem system;
	
	@Test
	public void test() {
		DirectedGraph grafo = new DirectedGraph("3;0 2;0 1;1 2");
		system = new SoftwareSystem(grafo);
		assertNotNull(grafo);
		assertNotNull(system);
		assertEquals("Il grafo non ha cicli mi aspetto false",false,system.hasCycle());
		grafo = new DirectedGraph("6;0 2;0 1;2 4;2 3;3 5;4 0");
		system = new SoftwareSystem(grafo);
		assertEquals("Il grafo ha cicli, mi aspetto true",true,system.hasCycle());
		grafo = new DirectedGraph("5;3 2;3 4;4 2");
		system = new SoftwareSystem(grafo);
		assertEquals("Il grafo non ha cicli mi aspetto false",false,system.hasCycle());

		grafo = new DirectedGraph("3;0 1;1 2;2 0");
		system = new SoftwareSystem(grafo);
		assertEquals("Il grafo ha cicli mi aspetto true",true,system.hasCycle());
	}

}
