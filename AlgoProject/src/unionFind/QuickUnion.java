package unionFind;

public class QuickUnion implements UnionFind {
	

	int[] insieme;
	int numInsiemi;
	
	public QuickUnion(int numeroOggetti) {
		numInsiemi = numeroOggetti;
		insieme = new int[numeroOggetti];
		for(int i=0;i<insieme.length;i++) {
			insieme[i] = i;
		}
	}

	@Override
	public UnionFind create(int size) {
		QuickUnion uf = new QuickUnion(size);
		return uf;
	}

	@Override
	public void union(int i, int j) {
		if(insieme[j]!=j || insieme[i]!=i) throw new IllegalArgumentException("Gli elementi non sono rappresentanti");
		if(i!=j) {
			if(insieme[i]==i && insieme[j]==j) {
				insieme[j] = i;
				numInsiemi--;
			}
		}
	}

	@Override
	public int find(int i) {
		while(true) {
			if(insieme[i]==i) break;
			i = insieme[i];
		}
		return insieme[i];
	}

	@Override
	public int[] getInsieme() {
		return insieme;
	}
	
	@Override
	public int getNumberOfSets() {
		return numInsiemi;
	}
}
