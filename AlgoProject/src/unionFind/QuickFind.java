package unionFind;

public class QuickFind implements UnionFind{
	
	int[] insieme;
	int numInsiemi;
	
	public QuickFind(int numeroOggetti) {
		numInsiemi = numeroOggetti;
		insieme = new int[numeroOggetti];
		for(int i=0;i<insieme.length;i++) {
			insieme[i] = i;
		}
	}

	@Override
	public UnionFind create(int size) {
		QuickFind uf = new QuickFind(size);
		return uf;
	}

	@Override
	public void union(int i, int j) {
		if(insieme[j]!=j || insieme[i]!=i) throw new IllegalArgumentException("Gli elementi non sono rappresentanti");
		if(i!=j) {
			for(int k = 0;k<insieme.length;k++) {
				if(insieme[k]==j) {
					insieme[k] = i;
					numInsiemi--;
				}
			}
		}
	}

	@Override
	public int find(int i) {
		return insieme[i];
	}
	
	@Override
	public int[] getInsieme() {
		return insieme;
	}

	@Override
	public int getNumberOfSets() {
		return numInsiemi;
	}
	
}
