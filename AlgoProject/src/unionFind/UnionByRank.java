package unionFind;

public class UnionByRank implements UnionFind {

	int[] insieme;
	int numInsiemi;
	int[] rank;
	
	public UnionByRank(int numeroOggetti) {
		numInsiemi = numeroOggetti;
		insieme = new int[numeroOggetti];
		for(int i=0;i<insieme.length;i++) {
			insieme[i] = i;
		}
		rank = new int[numeroOggetti];
	}

	@Override
	public UnionFind create(int size) {
		UnionByRank uf = new UnionByRank(size);
		return uf;
	}

	@Override
	public void union(int i, int j) {
		if(insieme[j]!=j || insieme[i]!=i) throw new IllegalArgumentException("Gli elementi non sono rappresentanti");
		if(i!=j) {
			if(rank[i]>=rank[j]) {
				insieme[j] = i;
				rank[i]++;
				numInsiemi--;
			}
			else {
				insieme[i] = j;
				rank[j]++;
				numInsiemi--;
			}
		}
	}

	@Override
	public int find(int i) {
		while(true) {
			if(insieme[i]==i) break;
			i = insieme[i];
		}
		return insieme[i];
	}

	@Override
	public int[] getInsieme() {
		return insieme;
	}
	
	@Override
	public int getNumberOfSets() {
		return numInsiemi;
	}
}
