package unionFind;

public interface UnionFind {
	
	public abstract UnionFind create(int size); // crea una nuova collezione
	
	public abstract void union(int i, int j);	// a e b sono rappresentanti di insiemi, il metodo fa l'unione
	
	public abstract int find(int i);	// restituisce il rappresentante dell'insieme a cui appartiene i
	
	public abstract int getNumberOfSets();	// restituisce il numero degli insiemi distinti presenti al momento
	
	public abstract int[] getInsieme();

}
