package unionFind;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestUnionByRank {

	@Test
	public void test() {
		UnionByRank qf = new UnionByRank(4);
		for(int i=0;i<4;i++) {
			System.out.println(qf.getInsieme()[i]);
		}
		assertEquals(4,qf.getNumberOfSets());
		assertEquals(2,qf.find(2));
		qf.union(2, 3);
		assertEquals(1,qf.rank[2]);
		assertEquals(0,qf.rank[3]);
		assertEquals(3,qf.getNumberOfSets());
		for(int i=0;i<4;i++) {
			System.out.println(qf.getInsieme()[i]);
		}
		assertEquals(2,qf.find(2));
		assertEquals(2,qf.find(3));
		assertEquals(1,qf.find(1));
		assertEquals(0,qf.find(0));
		qf.union(2, 2);
		assertEquals(3,qf.getNumberOfSets());
		qf.union(1, 1);
		assertEquals(3,qf.getNumberOfSets());
		qf.union(0, 2);
		assertEquals(2,qf.getNumberOfSets());
		for(int i=0;i<4;i++) {
			System.out.println(qf.getInsieme()[i]);
		}
		assertEquals(2,qf.rank[2]);
		assertEquals(0,qf.rank[0]);
		qf.union(1, 2);
		for(int i=0;i<4;i++) {
			System.out.println(qf.getInsieme()[i]);
		}
		assertEquals(2,qf.find(3));
		assertEquals(1,qf.getNumberOfSets());
		assertEquals(3,qf.rank[2]);
		assertEquals(0,qf.rank[1]);
	}

}
