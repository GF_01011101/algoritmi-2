package algoritmoBFS;

import java.util.ArrayList;

import it.uniupo.graphLib.GraphInterface;

public class BFS {
	
	GraphInterface grafo; //per memorizzare il grafo su cui si lavora
	boolean[] scoperto; //per memorizzare i nodi scoperti: scoperti[2]=true se il nodo 2 e' stato scoperto
	ArrayList<Integer> nodiVisitatiInOrdine; //elenco dei nodi nell'ordine in cui sono stati visitati
	int[] distanza; //distanza[v] = distanza del nodo v dalla sorgente
	ArrayList<Integer> coda;
	GraphInterface alberoBFS;
	
	/****************************
	 * Questo e' il costruttore
	 ****************************/
	public BFS(GraphInterface grafoInInput){
		this.grafo = grafoInInput;
		this.scoperto = new boolean[grafoInInput.getOrder()];
		this.distanza = new int[grafoInInput.getOrder()];
		this.nodiVisitatiInOrdine = new ArrayList<Integer>();
		this.coda = new ArrayList<Integer>();
	}

	private void init() {
		for(int i=0;i<distanza.length;i++) {
			distanza[i] = -1;
			scoperto[i] = false;
		}
		coda.clear();
		nodiVisitatiInOrdine.clear();
		alberoBFS = grafo.create();
	}
	
	/********
	 *  Il metodo visitaBFS(int sorgente) fa una visita BFS dalla sorgente, ma non restituisce niente:
	 *  modifica i valori delle opportune variabili di istanza
	 */
	 private void visitaBFS(int sorgente) {
		 if(sorgente>=grafo.getOrder() || sorgente<0) throw new IllegalArgumentException("Argomento fuori dal range");
		 distanza[sorgente] = 0;
		 scoperto[sorgente] = true;
	     coda.add(sorgente);
	     nodiVisitatiInOrdine.add(sorgente);
	     while(!coda.isEmpty()) {
	    	 Integer u = coda.remove(0);
	    	 for(Integer v : grafo.getNeighbors(u)) {
	    		 if(!scoperto[v]) {
	    			 coda.add(v);
	    			 distanza[v] = distanza[u]+1;
	    			 nodiVisitatiInOrdine.add(v);
	    			 scoperto[v] = true;
	    			 alberoBFS.addEdge(u,v);
	    		 }
	    	 }
	    }    
	 }
	 
	 public boolean isConnected() {
		 init();
		 visitaBFS(0);
		 for(int i=0;i<grafo.getOrder();i++) {
			 if(!scoperto[i]) return false;
		 }
		 return true;
	 }
	 
	 private void visitaBFScompleta() {
		 init();
		 for(int i=0;i<grafo.getOrder();i++) {
			 if(!scoperto[i]) {
				 visitaBFS(i);
			 }
		 }
	 }
	 
	 public ArrayList<Integer> getNodesInOrderOfVisit(int sorgente){
		 init();
		 visitaBFS(sorgente);
		 return nodiVisitatiInOrdine; 
	 }
	 
	 public GraphInterface getBFStree(int sorgente) {
		 init();
		 visitaBFS(sorgente);
		 return alberoBFS;
	 }
	 
	 public GraphInterface getForest() {
		 visitaBFScompleta();
		 return alberoBFS;
	 }
	 
	 public int[] getDistance(int sorgente) { 
		 init();
		 visitaBFS(sorgente);
		 return distanza; 
	 }
	 

}
