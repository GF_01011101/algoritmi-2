package algoritmoMSI;

import java.util.ArrayList;

public class MSI {
	
	int[] peso;
	int[] memoization;
	
	public MSI(int[] pesi) {
		this.peso = pesi;
		memoization = new int[pesi.length];
	}
	
	/**
	 * Restituisce il peso del massimo sottoinsieme indipendente
	 * @return peso del MSI
	 */
	public int getMaxVal() {
		memoization[0] = peso[0];
		for(int i=1;i<peso.length;i++) {
			if(i==1) {
				memoization[i] = Math.max(memoization[i-1], peso[i]);
			}
			else {
				memoization[i] = Math.max(memoization[i-1], memoization[i-2]+peso[i]);
			}
		}
		return memoization[peso.length-1];
	}
	
	
	public int getMaxValRicorsivoInefficiente(int n) {
		if(n==0) return peso[0];
		else if(n==1) return Math.max(getMaxValRicorsivoInefficiente(n-1), peso[n]);
		else return Math.max(getMaxValRicorsivoInefficiente(n-1), getMaxValRicorsivoInefficiente(n-2)+peso[n]);

	}
	
	
	/**
	 * Restituisce la soluzione ottimale
	 * @return elenco dei nodi che rappresentano la soluzione ottimale al problema
	 */
	public ArrayList<Integer> getOptSol(){	
		ArrayList<Integer> sol = new ArrayList<Integer>();
		getMaxVal();
		int i = peso.length-1;
		while(i>0) {
			if(i==1) {
				if(memoization[i-1]>peso[i]) i--;
				else {
					sol.add(i);
					i-=2;
				}
			}else {
				if(memoization[i-1]>(memoization[i-2]+peso[i])) i--;
				else {
					sol.add(i);
					i-=2;
				}
			}
			
		}
		if(i==0) sol.add(0);
		return sol;
	}
}
