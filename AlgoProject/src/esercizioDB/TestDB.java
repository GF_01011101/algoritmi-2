package esercizioDB;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestDB {

	@Test
	public void test() {
		int [] dim = {3, 2, 4};
		double[] time = {2, 5, 1}; 
		DB db = new DB(dim,time);
		assertEquals(3,db.memorize(10)[0]);
		assertEquals(2,db.memorize(10)[1]);
		assertEquals(4,db.memorize(10)[2]);
		assertEquals(3,db.memorize(10).length);
		assertEquals(8,db.timeSaved(10),0.00001);
	}
	
	@Test
	public void test2() {
		int[] dim = new int[]{70, 12, 3}; 
		double[] time = new double[]{10, 2, 0.3};
		DB test = new DB(dim,time);
		assertTrue(0.5==test.timeSaved(3));
		assertEquals(0,test.memorize(3)[0]);
		assertEquals(3,test.memorize(3)[1]);
		assertEquals(0,test.memorize(3)[2]);	
	}
	
	@Test
	public void test3() {
		int[] dim = {4, 3, 4};
		double[] time = {5, 3, 1};
		
		DB test = new DB(dim,time);
		assertTrue(8.75 == test.timeSaved(10));
		int[] arr = test.memorize(10);
		assertEquals(3,arr.length);
		assertEquals(4,arr[0]);
		assertEquals(3,arr[1]);
		assertEquals(3,arr[2]);
	}

	@Test (expected = IllegalArgumentException.class) 
	public void test1Exc() {
		int[] dim = {3, 2, 4};
		double[] time = {2, 5, 1};
		
		DB test = new DB(dim,time);
		assertTrue(8 == test.timeSaved(-10));
	}

	@Test (expected = IllegalArgumentException.class)
	public void test1Mem() {
		int[] dim = {3, 2, 4};
		double[] time = {2, 5, 1};
		
		DB test = new DB(dim,time);
		test.memorize(-10);
	}
	

}
