package esercizioDB;

import it.uniupo.algoTools.MaxHeap;

public class DB {
	
	int[] dim;
	double[] time;
	double[] dose;
	int[] q;
	
	public DB(int[] dim, double[] time) {
		this.dim = dim;
		this.time = time;
		dose = new double[dim.length];
		q = new int[dim.length];
	}
	
	private double ripristinoDB(int memSpace) {
		if(memSpace<0) throw new IllegalArgumentException("Inserire solo valori non negativi");
		MaxHeap<Integer,Double> heap = new MaxHeap<Integer,Double>();
		for(int i=0;i<dim.length;i++) {
			heap.add(i, time[i]/dim[i]);
			dose[i] = 0;
			q[i] = 0;
		}
		double tempo = 0;
		while(memSpace>0 && !heap.isEmpty()) {
			Integer db = heap.extractMax();
			if(dim[db]<=memSpace) {
				dose[db] = 1;
				tempo += time[db];
				q[db] = dim[db];
				memSpace -= dim[db];
			}
			else {
				dose[db] = (double) memSpace / (double) dim[db];
				q[db] = memSpace;
				tempo += time[db] * dose[db];
				memSpace = 0;
			}
		}
		return tempo;
	}

	
	public int[] memorize(int memSpace) {
		ripristinoDB(memSpace);
		return q;
	}
	
	public double timeSaved(int memSpace) {
		return ripristinoDB(memSpace);
	}
}

