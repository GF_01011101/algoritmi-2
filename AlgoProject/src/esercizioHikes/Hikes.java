package esercizioHikes;

import it.uniupo.algoTools.MinHeap;
import it.uniupo.algoTools.UnionByRank;
import it.uniupo.algoTools.UnionFind;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class Hikes {

	UndirectedGraph g;
	
	public Hikes(UndirectedGraph rifugi) {
		this.g = rifugi;
	}

	public int minDistanza(int numZones) {
		if(numZones<0) throw new IllegalArgumentException();
		int spaziamento, x, y;
		MinHeap<Edge,Integer> heap = new MinHeap<Edge,Integer>();
		addEdges(heap);
		UnionFind uf = new UnionByRank(g.getOrder());
		while(!heap.isEmpty() && uf.getNumberOfSets()>numZones){
			Edge uv = heap.extractMin();
			Integer u = uv.getTail();
			Integer v = uv.getHead();
			if(uf.find(u)!=uf.find(v)) {
				uf.union(uf.find(u), uf.find(v));
			}
		}
		do {
			Edge xy = heap.extractMin();
			x = xy.getTail();
			y = xy.getHead();
			spaziamento = xy.getWeight();
		}while(uf.find(x)==uf.find(y));
		return spaziamento;
	}
	
	
	private void addEdges(MinHeap<Edge,Integer> heap) {
		for(int i=0;i<g.getOrder();i++) {
			for(Edge uv : g.getOutEdges(i)) {
				heap.add(uv, uv.getWeight());
			}
		}
	}

}
