package esercizioHikes;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniupo.graphLib.UndirectedGraph;

public class TestHikes {

	@Test
	public void test1() {
		UndirectedGraph rifugi = new UndirectedGraph("5;0 1 13;0 2 16;0 3 19;0 4 9;1 2 7;1 3 14;1 4 22;2 3 12;2 4 15;3 4 26");
		Hikes h = new Hikes(rifugi);
		int numZones = 3;
		assertEquals(12,h.minDistanza(numZones));
	}
	
	@Test
	public void test2() {
		UndirectedGraph rifugi = new UndirectedGraph("5;0 1 13;0 2 16;0 3 19;0 4 9;1 2 7;1 3 14;1 4 22;2 3 12;2 4 15;3 4 26");
		Hikes h = new Hikes(rifugi);
		int numZones = 2;
		assertEquals(13,h.minDistanza(numZones));
	}

	@Test (expected = IllegalArgumentException.class)
	public void test() {
		UndirectedGraph rifugi = new UndirectedGraph("5;0 1 13;0 2 16;0 3 19;0 4 9;1 2 7;1 3 14;1 4 22;2 3 12;2 4 15;3 4 26");
		Hikes h = new Hikes(rifugi);
		int numZones = -4;
		assertEquals(12,h.minDistanza(numZones));
	}
	
	
}
