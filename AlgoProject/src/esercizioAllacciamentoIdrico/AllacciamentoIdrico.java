package esercizioAllacciamentoIdrico;

import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class AllacciamentoIdrico {
	
	UndirectedGraph g;
	int[][] costoScavo;
	int costoTubo;
	int puntoAllacciamento;
	boolean[] scopertoC;
	boolean[] scopertoP;
	int[] costoP;
	MinHeap<Edge,Integer> heapP;
	MinHeap<Edge,Integer> heapC;
	
	public AllacciamentoIdrico(UndirectedGraph mappa, int[][] costoScavo, int costoTubo, int puntoAllacciamento) {
		this.g = mappa;
		this.costoScavo = costoScavo;
		this.costoTubo = costoTubo;
		this.puntoAllacciamento = puntoAllacciamento;
		scopertoC = new boolean[g.getOrder()];
		scopertoP = new boolean[g.getOrder()];
		costoP = new int[g.getOrder()];
	}
	
	private void initP() {
		for(int i=0;i<scopertoP.length;i++) {
			scopertoP[i] = false;
			costoP[i] = -1;
		}
		heapP = new MinHeap<Edge,Integer>();
	}
	
	public UndirectedGraph progettoProprietari() {
		initP();
		UndirectedGraph alberoProprietari = (UndirectedGraph)g.create();
		scopertoP[puntoAllacciamento] = true;
		costoP[puntoAllacciamento] = 0;
		for(Edge su : g.getOutEdges(puntoAllacciamento)) {
			heapP.add(su, costoP[puntoAllacciamento]+(costoTubo*su.getWeight()));
		}
		while(!heapP.isEmpty()) {
			Edge uv = heapP.extractMin();
			Integer u = uv.getTail();
			Integer v = uv.getHead();
			if(!scopertoP[v]) {
				scopertoP[v] = true;
				costoP[v] = costoP[u]+(costoTubo*uv.getWeight());
				alberoProprietari.addEdge(uv);
				for(Edge vw : g.getOutEdges(v)) {
					if(!scopertoP[vw.getHead()]) {
						heapP.add(vw, costoP[v]+(costoTubo*vw.getWeight()));
					}
				}
			}
		}
		return alberoProprietari;
	}
	
	public UndirectedGraph progettoComune() {
		initC();
		UndirectedGraph alberoComune = (UndirectedGraph)g.create();
		scopertoC[puntoAllacciamento] = true;
		for(Edge su : g.getOutEdges(puntoAllacciamento)) {
			heapC.add(su, costoScavo[su.getTail()][su.getHead()]);
		}
		while(!heapC.isEmpty()) {
			Edge uv = heapC.extractMin();
			Integer v = uv.getHead();
			if(!scopertoC[v]) {
				scopertoC[v] = true;
				alberoComune.addEdge(uv);
				for(Edge vw : g.getOutEdges(v)) {
					if(!scopertoC[vw.getHead()]) {
						heapC.add(vw, costoScavo[vw.getTail()][vw.getHead()]);
					}
				}
			}
		}
		return alberoComune;
	}

	private void initC() {
		for(int i=0;i<scopertoC.length;i++) {
			scopertoC[i] = false;
		}
		heapC = new MinHeap<Edge,Integer>();
	}
	
}
