package esercizioAllacciamentoIdrico;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import it.uniupo.graphLib.UndirectedGraph;

public class TestAllacciamentoIdrico {
	
	AllacciamentoIdrico allId;
	
	@Test
	public void testCreate() {
		UndirectedGraph mappa = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
		int[][] costoScavo = {{0,6,-1,8},{6,0,7,-1},{-1,7,0,10},{8,-1,10,0}};
		int costoTubo = 3;
		int puntoAll = 1;
		allId = new AllacciamentoIdrico(mappa, costoScavo, costoTubo,puntoAll);
		
	}

	@Test
	public void test01() {
		System.out.println("Primo Test ---------------\n");
		UndirectedGraph mappa = new UndirectedGraph("4;1 2 2;1 0 5;2 3 3; 0 3 1");
		int[][] costoScavo = {{0,6,-1,8},{6,0,7,-1},{-1,7,0,10},{8,-1,10,0}};
		int costoTubo = 3;
		int puntoAll = 1;
		allId = new AllacciamentoIdrico(mappa, costoScavo, costoTubo,puntoAll);
		assertTrue(allId.progettoComune().hasEdge(0,3));
		assertTrue(!allId.progettoComune().hasEdge(2,3));
		assertTrue(allId.progettoProprietari().hasEdge(2,3));
		assertTrue(!allId.progettoProprietari().hasEdge(0,3));
		//assertEquals(2,allId.speseExtraComune());
		//assertEquals(3,allId.speseExtraProprietario(3));
		System.out.println("Progetto Comune:\n" + allId.progettoComune());
		System.out.println("Progetto Proprietari:\n" + allId.progettoProprietari());
		System.out.println("\nSecondo Test ---------------\n");
		mappa = new UndirectedGraph("8;0 2 10;0 4 20;0 5 20;0 6 18;1 4 3;1 2 5;2 3 8;3 7 10;7 6 12;6 5 10");
		int[][] costoScavo2 = {{-1,-1,3,-1,10,5,7,-1},{-1,-1,5,-1,1,-1,-1,-1},{3,5,-1,7,-1,-1,-1,-1},{-1,-1,7,-1,-1,-1,-1,6},{10,1,-1,-1,-1,-1,-1,-1},{5,-1,-1,-1,-1,-1,2,-1},{7,-1,-1,-1,-1,2,-1,4},{-1,-1,-1,6,-1,-1,4,-1}};
		costoTubo = 1;
		puntoAll = 0;
		allId = new AllacciamentoIdrico(mappa,costoScavo2,costoTubo,puntoAll);
		assertTrue(allId.progettoComune().hasEdge(0,5));
		assertTrue(allId.progettoComune().hasEdge(5,6));
		assertTrue(!allId.progettoComune().hasEdge(0,6));
		System.out.println("Progetto Comune:\n" + allId.progettoComune());
		System.out.println("Progetto Proprietari:\n" + allId.progettoProprietari());
	}

}
