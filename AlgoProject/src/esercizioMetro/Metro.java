package esercizioMetro;

import java.util.ArrayList;

import it.uniupo.graphLib.UndirectedGraph;

public class Metro {
	
	UndirectedGraph mappa;
	boolean[] scoperto;
	ArrayList<Integer> elencoFermate;
	ArrayList<Integer> coda;
	int[] padre;
	
	public Metro(UndirectedGraph mappa) {
		this.mappa = mappa;
	}
	
	private void init(int sorgente) {
		scoperto = new boolean[mappa.getOrder()];
		elencoFermate = new ArrayList<Integer>();
		padre = new int[mappa.getOrder()];
		coda = new ArrayList<Integer>();
		for(int i=0;i<padre.length;i++) {
			padre[i] = -1;
		}
	}
	
	private void visitaBFS(int sorgente, int destinazione) {
		scoperto[sorgente] = true;
		coda.add(sorgente);
		while(!coda.isEmpty()) {
			Integer u = coda.remove(0);
			for(Integer v : mappa.getNeighbors(u)) {
				if(!scoperto[v]) {
					coda.add(v);
					scoperto[v] = true;
					padre[v] = u;
					
				}
				if(v==destinazione) return;
			}
		}
		
	}
	
	public ArrayList<Integer> fermate(int stazPartenza, int stazArrivo){
		if(stazPartenza<0 || stazPartenza>=mappa.getOrder() || stazArrivo<0 || stazArrivo>=mappa.getOrder())
			throw new IllegalArgumentException("Le stazioni sono illegali");
		init(stazPartenza);
		visitaBFS(stazPartenza, stazArrivo);
		if(!scoperto[stazArrivo]) return null;
		int i = stazArrivo;
		while(i!=stazPartenza) {
			elencoFermate.add(0, i);
			i = padre[i];
		}
		elencoFermate.add(0, stazPartenza);
		return elencoFermate;
	}
}
