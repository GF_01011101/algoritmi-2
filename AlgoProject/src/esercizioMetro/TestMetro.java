package esercizioMetro;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniupo.graphLib.UndirectedGraph;

public class TestMetro {

	Metro metro;
	UndirectedGraph mappa;
	
	@Test
	public void test() {
		mappa = new UndirectedGraph("5;0 2;0 1;2 3;3 4;1 4");
		metro = new Metro(mappa);
		assertNotNull(mappa);
		assertNotNull(metro);
		assertEquals("Il percorso prevede tre stazioni comprese la prima e l'ultima",3,metro.fermate(0, 4).size());
		System.out.println(metro.elencoFermate);
	}
}
