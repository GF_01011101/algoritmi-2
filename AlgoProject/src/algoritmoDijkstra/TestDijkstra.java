package algoritmoDijkstra;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniupo.graphLib.GraphInterface;
import it.uniupo.graphLib.UndirectedGraph;

public class TestDijkstra {

	GraphInterface g;
	Dijkstra dj;
	
	@Test
	public void testDijkstra() {
		g = new UndirectedGraph("7; 1 2 5; 1 3 2; 1 4 3; 2 6 3; 6 0 2; 4 0 4; 3 5 2; 5 2 1");
		dj = new Dijkstra(g);
		int[] distanza = dj.getDistance(1);
		for(int i=0;i<g.getOrder();i++)
			System.out.println(distanza[i]);
		System.out.println(dj.ordineVisita);
		assertEquals(7,distanza[0]);
	}

}
