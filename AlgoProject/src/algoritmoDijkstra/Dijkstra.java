package algoritmoDijkstra;

import it.uniupo.graphLib.*;

import java.util.ArrayList;

import it.uniupo.algoTools.*;

public class Dijkstra {
	GraphInterface g;
	boolean[] scoperto;
	int[] distanza;
	MinHeap<Edge,Integer> heap;
	ArrayList<Integer> ordineVisita;
	
	public Dijkstra(GraphInterface gInput) {
		this.g = gInput;
		this.scoperto = new boolean[g.getOrder()];
		this.distanza = new int[g.getOrder()];
		this.ordineVisita = new ArrayList<Integer>();
	}
	
	public int[] getDistance(int sorgente) {
		if(sorgente<0 || sorgente>=g.getOrder()) throw new IllegalArgumentException("l'input non è un nodo del grafo");
		init();
		visitaBFS(sorgente);
		return distanza;
	}

	private void visitaBFS(int sorgente) {
		scoperto[sorgente] = true;
		distanza[sorgente] = 0;
		ordineVisita.add(sorgente);
		for(Edge uv : g.getOutEdges(sorgente)) {
			heap.add(uv, distanza[sorgente]+uv.getWeight());
		}
		while(!heap.isEmpty()) {
			Edge vw = heap.extractMin();
			Integer w = vw.getHead();
			if(!scoperto[w]) {
				scoperto[w] = true;
				distanza[w] = distanza[vw.getTail()]+vw.getWeight();
				ordineVisita.add(w);
				for(Edge wz : g.getOutEdges(w)) {
					if(!scoperto[wz.getHead()])
						heap.add(wz, distanza[wz.getTail()]+wz.getWeight());
				}
			}
		}
	}
	
	private void init() {
		for(int i=0;i<g.getOrder();i++) {
			distanza[i] = -1;
			scoperto[i] = false;
		}
		ordineVisita.clear();
		heap = new MinHeap<Edge,Integer>();
	}
}
