package algoritmoBellmanFord;

import java.util.ArrayList;

import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphUtils;

public class BellmanFord {
	DirectedGraph g;
	int sorgente;
	Double[][] distanza;
	int[] padre;
	boolean ciclo;
	
	public BellmanFord(DirectedGraph gInput, int sorgente) {
		this.g = gInput;
		this.sorgente = sorgente;
	}
	
	private void BFord() {
		int n = g.getOrder();
		distanza = new Double[n+1][n];
		padre = new int[n];
		DirectedGraph trasposto = GraphUtils.reverseGraph(g);
		for(int v=0;v<n;v++) {
			if(v==sorgente) distanza[0][v] = 0.0;
			else distanza[0][v] = Double.POSITIVE_INFINITY;
			padre[v] = -1;
		}
		for(int i=1;i<=n;i++) {
			Edge minimo;
			int m;
			for(int v=0;v<n;v++) {
				minimo = null;
				m = 1;
				for(Edge wv : trasposto.getOutEdges(v)) {
					if(m==1) minimo = wv;
					else {
						if(wv.getHead()+wv.getWeight()< minimo.getHead()+minimo.getWeight()) 
							minimo = wv;
					}
					m++;
				}
				if(minimo==null) distanza[i][v] = distanza[i-1][v];
				else {
					distanza[i][v] = Math.min(distanza[i-1][v], distanza[i-1][minimo.getHead()]+minimo.getWeight());
					if(distanza[i][v]==distanza[i-1][minimo.getHead()]+minimo.getWeight()) {
						padre[v] = minimo.getHead();
					}
				}
			}
		}
	}
	
	
	/**
	 * Restituisce la distanza di u dalla sorgente oppure -infinito se esiste un ciclo negativo
	 */
	public Double getDist(int u) {
		if(hasNegCycle()) return Double.NEGATIVE_INFINITY;
		return distanza[g.getOrder()][u];
	}
	
	/**
	 * Restituisce true se esiste un ciclo negativo
	 */
	public boolean hasNegCycle() {
		BFord();
		int n = g.getOrder();
		for(int v=0;v<n;v++) {
			if(distanza[n][v]<distanza[n-1][v]) return true;
		}
		return false;
	}
	
	public ArrayList<Integer> getPath(int u){
		if(hasNegCycle()) return null;
		ArrayList<Integer> percorso = new ArrayList<Integer>();
		if(padre[u]==-1) {
			percorso.add(-1);
			return percorso;
		}
		int ultimo = u;
		percorso.add(ultimo);
		while(ultimo!=sorgente) {
			ultimo = padre[ultimo];
			percorso.add(0,ultimo);
		}
		return percorso;
	}
}
