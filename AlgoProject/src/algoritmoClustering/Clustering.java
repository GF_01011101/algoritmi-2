package algoritmoClustering;

import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;
import unionFind.UnionByRank;
import unionFind.UnionFind;

public class Clustering {
	UndirectedGraph g;
	int numCluster;
	
	public Clustering(UndirectedGraph gInput, int numCluster) {
		this.g = gInput;
		this.numCluster = numCluster;
	}
	
	public int spaziamento() {
		int spaz,x,y;
		MinHeap<Edge,Integer> heap = new MinHeap<Edge,Integer>();
		addEdge(heap);
		UnionFind uf = new UnionByRank(g.getOrder());
		while(!heap.isEmpty() && uf.getNumberOfSets()>numCluster) {
			Edge uv = heap.extractMin();
			Integer u = uv.getTail();
			Integer v = uv.getHead();
			if(uf.find(u)!=uf.find(v)) {
				uf.union(uf.find(u), uf.find(v));
			}
		}
		do {
			Edge xy = heap.extractMin();
			x = xy.getTail();
			y = xy.getHead();
			spaz = xy.getWeight();
		}while(uf.find(x)==uf.find(y));
		return spaz;
	}
	
	public boolean sameCluster(int a, int b) {
		MinHeap<Edge,Integer> heap = new MinHeap<Edge,Integer>();
		addEdge(heap);
		UnionFind uf = new UnionByRank(g.getOrder());
		while(!heap.isEmpty() && uf.getNumberOfSets()>numCluster) {
			Edge uv = heap.extractMin();
			Integer u = uv.getTail();
			Integer v = uv.getHead();
			if(uf.find(u)!=uf.find(v)) {
				uf.union(uf.find(u), uf.find(v));
			}
		}
		if(uf.find(a)==uf.find(b)) return true;
		return false;
	}
		
	private void addEdge(MinHeap<Edge,Integer> heap) {
		for(int i=0;i<g.getOrder();i++) {
			for(Edge uv : g.getOutEdges(i)) {
				heap.add(uv, uv.getWeight());
			}
		}
	}
}
