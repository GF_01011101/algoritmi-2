package algoritmoClustering;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniupo.graphLib.UndirectedGraph;

public class TestClustering {

	@Test
	public void test() {
		UndirectedGraph g = new UndirectedGraph("4;0 2 5;0 1 7;0 3 3;1 3 4;3 2 8;1 2 2");
		Clustering clustering = new Clustering(g,2);
		int spaz = clustering.spaziamento();
		assertEquals(4,spaz);
	}

}
