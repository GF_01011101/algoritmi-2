package algoritmoFloydWarshall;

import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;

public class FloydWarshall {

	DirectedGraph g;
	Double[][] distanza;
	
	public FloydWarshall(DirectedGraph g) {
		this.g = g;
	}

	private void floydWarshall() {
		int n = g.getOrder();
		distanza = new Double[n][n];
		for(int u=0;u<n;u++) {
			for(int v=0;v<n;v++) {
				if(u==v) distanza[u][v] = 0.0;
				else if(g.hasEdge(new Edge(u,v))){
					for(Edge uv : g.getOutEdges(u)) {
						if(uv.getHead()==v) {
							distanza[u][v] = (double)uv.getWeight();
						}
					}	
				}
				else {
					distanza[u][v] = Double.POSITIVE_INFINITY;
				}	
			}
		}
		for(int k=1;k<n;k++) {
			for(int u=0;u<n;u++) {
				for(int v=0;v<n;v++) {
					distanza[u][v] = Math.min(distanza[u][v], distanza[u][k]+distanza[k][v]);
				}
			}
		}
	}
	
	
	public Double getDist(int u, int v) {
		if(hasNegCycle()) return Double.NEGATIVE_INFINITY;
		return distanza[u][v];
		
	}
	
	public boolean hasNegCycle() {
		floydWarshall();
		for(int i=0;i<g.getOrder();i++) {
			if(distanza[i][i]<0) return true; 
		}
		return false;
	}
	
	

}
