package algoritmoBellmanFordDAG;

import java.util.ArrayList;

import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphUtils;

public class BellmanFordDAG {
	DirectedGraph g;
	Double[] distanza;
	int[] padre;
	int sorgente;
	boolean[] scoperto;
	ArrayList<Integer> ordineTopologico;
	boolean ciclo;
	boolean[] terminato;
	
	public BellmanFordDAG(DirectedGraph gInput, int sorgente) {
		this.g = gInput;
		this.sorgente = sorgente;
	}
	
	private void visitaDFS(int sorg) {
		scoperto[sorg] = true;
		for(Edge uv : g.getOutEdges(sorg)) {
			Integer v = uv.getHead();
			if(!scoperto[v]) {
				visitaDFS(v);
			}
			else if(scoperto[v] && !terminato[v]) {
				ciclo = true;
			}
		}
		terminato[sorg] = true;
		ordineTopologico.add(0,sorg); 
	}
	
	private void visitaDFSCompleta() {
		init();
		for(int i=0;i<g.getOrder();i++) {
			if(!scoperto[i]) {
				visitaDFS(i);
			}
		}
	}
	
	private void BFordDAG() {
		visitaDFSCompleta();
		for(int v=0;v<g.getOrder();v++) {
			if(v==sorgente) distanza[v] = 0.0;
			else distanza[v] = Double.POSITIVE_INFINITY;
			padre[v] = -1;
		}
		DirectedGraph trasposto = GraphUtils.reverseGraph(g);
		for(Integer v : ordineTopologico) {
			int i = 1;
			Edge min = null;
			for(Edge wv : trasposto.getOutEdges(v)) {
				if(i==1) min = wv;
				else if(distanza[wv.getHead()]+wv.getWeight()<distanza[min.getHead()]+min.getWeight()) min = wv;
				i++;
			}
			if(min!=null) {
				distanza[v] = distanza[min.getHead()] + min.getWeight();
				padre[v] = min.getHead();
			}
		}
		
	}

	private void init() {
		int n = g.getOrder();
		distanza = new Double[n];
		scoperto = new boolean[n];
		terminato = new boolean[n];
		padre = new int[n];
		ciclo = false;
		ordineTopologico = new ArrayList<Integer>();
	}
	
	public Double getDist(int u) {
		if(hasCycle()) return Double.NEGATIVE_INFINITY;
		return distanza[u];
	}
	
	public ArrayList<Integer> getPath(int u){
		if(hasCycle()) return null;
		ArrayList<Integer> percorso = new ArrayList<Integer>();
		if(padre[u]==-1) {
			percorso.add(-1);
			return percorso;
		}
		int ultimo = u;
		percorso.add(ultimo);
		while(ultimo!=sorgente) {
			ultimo = padre[ultimo];
			percorso.add(0,ultimo);
		}
		return percorso;
	}
	
	public boolean hasCycle() {
		BFordDAG();
		return ciclo;
		
	}
}
