package esercizioAtletica;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestAtletica {

	@Test
	public void test1() {
		int discipline = 5;
		Atletica atletica = new Atletica(discipline);
		int[] at1 = {8,4,2,6,3};
		int[] at2 = {3,10,7,7,4};
		assertEquals(2,atletica.scelta(at1, at2));
	}
	
	@Test
	public void test2() {
		int discipline = 5;
		Atletica atletica = new Atletica(discipline);
		int[] at1 = {8,4,2,6,3};
		int[] at2 = {3,10,7,4,4};
		assertEquals(0,atletica.scelta(at1, at2));
	}

	@Test (expected = IllegalArgumentException.class)
	public void test3() {
		int discipline = 5;
		Atletica atletica = new Atletica(discipline);
		int[] at1 = {8,4,2,6,3,1};
		int[] at2 = {3,10,7,4,4};
		assertEquals(0,atletica.scelta(at1, at2));
	}
	
}
