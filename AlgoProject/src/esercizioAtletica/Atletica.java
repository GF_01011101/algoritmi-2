package esercizioAtletica;

public class Atletica {
	
	int numDiscipline;
	
	public Atletica(int numeroDiscipline) {
		this.numDiscipline = numeroDiscipline;
	}
	
	public int scelta(int[] rendAtleta1, int[] rendAtleta2) {
		if(rendAtleta1.length!=numDiscipline || rendAtleta2.length!=numDiscipline) 
			throw new IllegalArgumentException();
		int[] a1 = new int[numDiscipline];
		int[] a2 = new int[numDiscipline];
		a1[0] = rendAtleta1[0];
		a2[0] = rendAtleta2[0];
		for(int i=1;i<numDiscipline;i++) {
			if(i==1) {
				a1[i] = Math.max(a1[i-1], rendAtleta1[i]);
				a2[i] = Math.max(a2[i-1], rendAtleta2[i]);
			}
			else {
				a1[i] = Math.max(a1[i-1], a1[i-2] + rendAtleta1[i]);
				a2[i] = Math.max(a2[i-1], a2[i-2] + rendAtleta2[i]);	
			}
		}
		if(a1[numDiscipline-1] > a2[numDiscipline-1]) return 1;
		else if(a1[numDiscipline-1] < a2[numDiscipline-1]) return 2;
		else return 0;
	}
}
