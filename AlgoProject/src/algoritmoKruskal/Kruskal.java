package algoritmoKruskal;


import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;
import unionFind.*;

public class Kruskal {
	UndirectedGraph g;
	UndirectedGraph tree;

	public Kruskal(UndirectedGraph gInput) {
		this.g = gInput;
	}
	
	public UndirectedGraph getMST() {
		tree = (UndirectedGraph) g.create();
		MinHeap<Edge,Integer> heap = new MinHeap<Edge,Integer>();
		addEdge(heap);
		UnionFind uf = new UnionByRank(g.getOrder());
		while(!heap.isEmpty()) {
			Edge uv = heap.extractMin();
			Integer u = uv.getTail();
			Integer v = uv.getHead();
			if(uf.find(u)!=uf.find(v)) {
				tree.addEdge(uv);
				uf.union(uf.find(u), uf.find(v));
			}
		}
		return tree;
	}
	
	private void addEdge(MinHeap<Edge,Integer> heap) {
		for(int i=0;i<g.getOrder();i++) {
			for(Edge uv : g.getOutEdges(i))
				heap.add(uv, uv.getWeight());
		}
	}
}
