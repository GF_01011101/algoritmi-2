package algoritmoPrim;

import static org.junit.Assert.*;

import org.junit.Test;

import algoritmoKruskal.Kruskal;
import it.uniupo.graphLib.UndirectedGraph;

public class TestPrim {

	@Test
	public void test() {
		UndirectedGraph g = new UndirectedGraph("6;0 2 1;0 1 3;1 3 2;3 2 4;2 4 3;4 5 6;5 1 1");
		Prim p = new Prim(g);
		UndirectedGraph albero = p.getMST();
		System.out.println("Grafo di partenza\n"+g);
		System.out.println("Prim MST\n"+albero);
		assertEquals(5,albero.getEdgeNum());
		assertEquals(true,!albero.hasEdge(4,5) && !albero.hasEdge(2,3));
		Kruskal k = new Kruskal(g);
		UndirectedGraph alberoK = k.getMST();
		System.out.println("Kruskal MST\n"+alberoK);
		assertTrue(albero.getEdgeNum()==alberoK.getEdgeNum());
	}

}
