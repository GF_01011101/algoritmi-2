package algoritmoPrim;

import java.util.ArrayList;

import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.UndirectedGraph;

public class Prim {
	UndirectedGraph g;
	boolean[] scoperto;
	UndirectedGraph alberoMST;
	MinHeap<Edge,Integer> heap;
	ArrayList<Integer> ordineVisita;
	
	public Prim(UndirectedGraph gInput) {
		this.g = gInput;
		scoperto = new boolean[g.getOrder()];
	}
	
	private void init() {
		for(int i = 0;i<scoperto.length;i++) {
			scoperto[i] = false;
		}
		this.alberoMST = (UndirectedGraph)g.create();
		this.heap = new MinHeap<Edge,Integer>();
		this.ordineVisita = new ArrayList<Integer>();
	}
	
	private void prim(int sorgente) {
		scoperto[sorgente] = true;
		ordineVisita.add(sorgente);
		for(Edge su : g.getOutEdges(sorgente)) {
			heap.add(su, su.getWeight());
		}
		while(!heap.isEmpty()) {
			Edge uv = heap.extractMin();
			Integer v = uv.getHead();
			if(!scoperto[v]) {
				scoperto[v] = true;
				alberoMST.addEdge(uv);
				ordineVisita.add(v);
				for(Edge vw : g.getOutEdges(v)) {
					if(!scoperto[vw.getHead()])
						heap.add(vw, vw.getWeight());
				}
			}
		}
	}
	
	private void primCompleto() {
		init();
		for(int i=0;i<g.getOrder();i++) {
			if(!scoperto[i])
				prim(i);
		}
	}
	
	public UndirectedGraph getMST() {
		primCompleto();
		return alberoMST;
	}
}
