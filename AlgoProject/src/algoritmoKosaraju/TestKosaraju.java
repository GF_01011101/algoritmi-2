package algoritmoKosaraju;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniupo.graphLib.DirectedGraph;

public class TestKosaraju {

	DirectedGraph g;
	Kosaraju ko;
	
	@Test
	public void testPostVisit() {
		g = new DirectedGraph("5; 0 2; 2 0; 1 2; 4 0; 1 4; 4 3; 3 1");
		ko = new Kosaraju(g);
		assertTrue(ko.postVisit().indexOf(0)<ko.postVisit().indexOf(1));
		
		g = new DirectedGraph("5; 1 2; 2 1; 0 2; 4 1; 0 4; 4 3; 3 0");
		ko = new Kosaraju(g);
		assertTrue(ko.postVisit().indexOf(0)>ko.postVisit().indexOf(1));
	}
	
	@Test
	public void testGetSCC() {
		g = new DirectedGraph("1");
		ko = new Kosaraju(g);
		assertTrue(ko.postVisit().size()!=0);
		assertTrue(ko.getSCC()[0]==0);
		
		// test con un DAG
		g = new DirectedGraph("4; 2 0; 2 1; 0 3");
		ko = new Kosaraju(g);
		assertTrue(ko.postVisit().indexOf(3)==0);
		int[] componentiDiG = ko.getSCC();
		ko.trasposto(g);
		int[] componentiDiGPrimo = ko.getSCC();
		assertTrue(componentiDiG.equals(componentiDiGPrimo));
		
		g = new DirectedGraph("5; 0 2; 2 0; 1 2; 4 0; 1 4; 4 3; 3 1");
		ko = new Kosaraju(g);
		assertTrue(ko.postVisit().indexOf(0)<ko.postVisit().indexOf(1));
	}

}
