package algoritmoKosaraju;



import java.util.ArrayList;

import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphUtils;


public class Kosaraju {

	DirectedGraph g;
	boolean[] scoperto;
	ArrayList<Integer> nodiVisitatiPostOrdine;
	int contatoreCC;
	int[] CC;
	ArrayList<Integer> ordineTopologico;
	
	public Kosaraju(DirectedGraph g) {
	 	this.g = g;
	 	this.scoperto = new boolean[g.getOrder()];
	 	this.nodiVisitatiPostOrdine = new ArrayList<Integer>();
	 	this.CC = new int[g.getOrder()];
	 	this.ordineTopologico = new ArrayList<Integer>();
	}
	
	private void init() {
		for(int i=0;i<g.getOrder();i++) {
			scoperto[i] = false;
		}
		contatoreCC = 0;
		nodiVisitatiPostOrdine.clear();
		ordineTopologico.clear();
	}
	
	private void visitaDFS(int sorgente) {
		scoperto[sorgente] = true;
		CC[sorgente] = contatoreCC;
		for(Edge uv : g.getOutEdges(sorgente)) {
			Integer v = uv.getHead();
			if(!scoperto[v]) {
				visitaDFS(v);
			}
		}
		nodiVisitatiPostOrdine.add(sorgente); // ordine di fine visita
		ordineTopologico.add(0, sorgente); // ordine topologico (è l'ordine di fine visita inverso)
	}
	
	private void visitaDFSCompleta() {
		init();
		for(int i=0;i<g.getOrder();i++) {
			if(!scoperto[i]) {
				visitaDFS(i);
			}
			
		}
	}
	
	public ArrayList<Integer> postVisit(){
		visitaDFSCompleta();
		return nodiVisitatiPostOrdine;
	}
	
	public DirectedGraph trasposto(DirectedGraph grafo) {
		return GraphUtils.reverseGraph(grafo);
	}
	
	private ArrayList<Integer> copia(ArrayList<Integer> ot){
		ArrayList<Integer> otInverso = new ArrayList<Integer>();
		for(Integer i : ot) {
			otInverso.add(i);
		}
		return otInverso;
	}
	
	public int[] getSCC() {
		ArrayList<Integer> postOrdineInverso = copia(ordineTopologico);
		DirectedGraph gTrasposto = trasposto(g);
		g = gTrasposto;
		init();
		for(Integer i : postOrdineInverso) {
			if(!scoperto[i]) {
				visitaDFS(i);
				contatoreCC++;
			}
		}
		return CC;
	}
	
}
