package algoritmoZaino;

import java.util.ArrayList;

public class Zaino {
	
	int[] value;
	int[] volume;
	int capacity;
	int[][] valOpt;

	public Zaino(int[] value, int[] volume, int capacity) {
		this.value = value;
		this.volume = volume;
		this.capacity = capacity;	
	}
	
	/**
	 * Algoritmo dello zaino
	 */
	private void zaino() {
		valOpt = new int[value.length+1][capacity+1];
		for(int c=0;c<=capacity;c++) {
			valOpt[0][c] = 0;
		}
		for(int h=1;h<=value.length;h++) {
			for(int k=0;k<=capacity;k++) {
				if(volume[h-1]<=k) {
					valOpt[h][k] = Math.max(valOpt[h-1][k], valOpt[h-1][k-volume[h-1]]+value[h-1]);
				}
				else {
					valOpt[h][k] = valOpt[h-1][k];
				}
			}
		}
	}
	
	/**
	 * Restiuisce il valore della soluzione ottimale
	 * @return valore della soluzione ottimale
	 */
	public int getMaxVal() {
		zaino();
		return valOpt[value.length][capacity];
	}
	
	/**
	 * Restituisce la soluzione ottimale
	 * @return soluzione ottimale
	 */
	public ArrayList<Integer> getOptSol() {
		zaino();
		ArrayList<Integer> soluzione = new ArrayList<Integer>();
		int n = value.length;
		int c = capacity;
		while(n>0 && c>0) {
			if(valOpt[n][c]>valOpt[n-1][c]) {
				soluzione.add(0,n-1);
				n--;
				c = c - volume[n];
			}
			else {
				n--;
			}
		}
		return soluzione;
	}
}
