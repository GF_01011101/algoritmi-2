package esercizioVoli;

import java.util.ArrayList;

import it.uniupo.algoTools.MinHeap;
import it.uniupo.graphLib.DirectedGraph;
import it.uniupo.graphLib.Edge;
import it.uniupo.graphLib.GraphInterface;

public class Voli {
	
	DirectedGraph g;
	boolean[] scoperto;
	int[] distanza;
	int[] padre;
	ArrayList<Integer> coda;
	
	public Voli(DirectedGraph collegamenti) {
		g = collegamenti;
		scoperto = new boolean[g.getOrder()];
		distanza = new int[g.getOrder()];
		padre = new int[g.getOrder()];
		coda = new ArrayList<Integer>();
	}
	
	private void init() {
		for(int i=0;i<g.getOrder();i++) {
			scoperto[i] = false;
			distanza[i] = -1;
			padre[i] = -1;
		}
		coda.clear();
		
	}
	
	private void BFS(int partenza, int destinazione) {
		scoperto[partenza] = true;
		distanza[partenza] = 0;
		coda.add(partenza);
		while(!coda.isEmpty()) {
			Integer u = coda.remove(0);
			for(Edge uv : g.getOutEdges(u)) {
				Integer v = uv.getHead();
				if(!scoperto[v]) {
					scoperto[v] = true;
					coda.add(v);
					padre[v] = u;
					distanza[v] = distanza[u]+uv.getWeight();
				}
				if(v==destinazione) return;
			}
		}
	}
	
	/*
	 * int tempo (int partenza, int destinazione) che restituisce il tempo totale di volo, 
	 * sulla tratta con il minimo numero di scali per raggiungere 
	 * la destinazione dall'aeroporto di partenza; restituisce -1 
	 * se destinazione non è raggiungibile da partenza e 
	 * lancia una java.lang.IllegalArgumentException 
	 * se partenza o destinazione non sono identificativi di aeroporti
	 * */
	public int tempo(int partenza, int destinazione) {
		if(partenza<0 || destinazione<0 || partenza>=g.getOrder() || destinazione>=g.getOrder())
			throw new IllegalArgumentException("Partenza o destinazione non sono aeroporti legali");
		init();
		BFS(partenza,destinazione);
		return (scoperto[destinazione]) ? distanza[destinazione] : -1;
		
	}
	
	
	/*int scali(int partenza, int destinazione) che restituisce il minimo numero di scali 
	 * necessario per raggiungere la destinazione dall'aeroporto di partenza; 
	 * restituisce -1 se destinazione non è raggiungibile da partenza e 
	 * lancia una java.lang.IllegalArgumentException 
	 * se partenza o destinazione non sono identificativi di aeroporti. 
	 * (Gli scali sono le soste intermedie tra partenza e desRnazione.)
	 */
	public int scali(int partenza, int destinazione) {
		if(partenza<0 || destinazione<0 || partenza>=g.getOrder() || destinazione>=g.getOrder())
			throw new IllegalArgumentException("Partenza o destinazione non sono aeroporti legali");
		init();
		BFS(partenza,destinazione);
		if(!scoperto[destinazione]) return -1;
		int sommaScali = 0;
		while(destinazione!=partenza) {
			sommaScali++;
			destinazione = padre[destinazione];
		}
		return (sommaScali>0) ? sommaScali-1 : sommaScali;
		
	}
	
	/*int tempoMInimo(int partenza, int destinazione) che restituisce il minimo tempo totale di volo 
	 * per raggiungere la destinazione dall'aeroporto di partenza; 
	 * restituisce -1 se destinazione non è raggiungibile da partenza e 
	 * lancia una java.lang.IllegalArgumentException 
	 * se partenza o destinazione non sono identificativi di aeroporti
	 */
	public int tempoMinimo(int partenza, int destinazione) {
		if(partenza<0 || destinazione<0 || partenza>=g.getOrder() || destinazione>=g.getOrder())
			throw new IllegalArgumentException("Partenza o destinazione non sono aeroporti legali");
		init();
		scoperto[partenza] = true;
		distanza[partenza] = 0;
		MinHeap<Edge,Integer> heap = new MinHeap<Edge,Integer>();
		for(Edge uv : g.getOutEdges(partenza)) {
			heap.add(uv, distanza[partenza]+uv.getWeight());
		}
		while(!heap.isEmpty()) {
			Edge vw = heap.extractMin();
			Integer w = vw.getHead();
			Integer v = vw.getTail();
			if(!scoperto[w]) {
				scoperto[w] = true;
				distanza[w] = distanza[v]+vw.getWeight();
				padre[w] = v;
				for(Edge wz : g.getOutEdges(w)) {
					if(!scoperto[wz.getHead()])
						heap.add(wz, distanza[w]+wz.getWeight());
				}
			}
		}
		
		return (scoperto[destinazione]) ? distanza[destinazione] : -1;
	}
	
	public ArrayList<Integer> elencoScali(int partenza, int destinazione){
		tempo(partenza,destinazione);
		if(!scoperto[destinazione]) return null;
		ArrayList<Integer> elenco = new ArrayList<Integer>();
		if(destinazione==partenza) return elenco;
		destinazione = padre[destinazione];
		while(destinazione!=partenza) {
			elenco.add(0,destinazione);
			destinazione = padre[destinazione];
			
		}
		return elenco;
	}
	
	public ArrayList<Edge> trattaVeloce(int partenza, int destinazione){
		tempoMinimo(partenza,destinazione);
		if(!scoperto[destinazione]) return null;
		ArrayList<Edge> percorsoVeloce = new ArrayList<Edge>();
		while(destinazione!=partenza) {
			Edge uv = new Edge(padre[destinazione],destinazione);
			percorsoVeloce.add(0,uv);
			destinazione = padre[destinazione];
		}
		return percorsoVeloce;
	}
	
	public GraphInterface getAlberoCamminiMinimi(int sorgente) {
		GraphInterface alberoDijkstra = g.create();
		MinHeap<Edge,Integer> heap = new MinHeap<Edge,Integer>();
		init();
		scoperto[sorgente] = true;
		distanza[sorgente] = 0;
		for(Edge su : g.getOutEdges(sorgente)) {
			heap.add(su, distanza[sorgente]+su.getWeight());
		}
		while(!heap.isEmpty()) {
			Edge uv = heap.extractMin();
			Integer u = uv.getTail();
			Integer v = uv.getHead();
			if(!scoperto[v]) {
				scoperto[v] = true;
				distanza[v] = distanza[u] + uv.getWeight();
				alberoDijkstra.addEdge(uv);
				for(Edge vw : g.getOutEdges(v)) {
					if(!scoperto[vw.getHead()])
						heap.add(vw, distanza[v]+vw.getWeight());
				}
			}
		}
		
		return alberoDijkstra;
	}
	
}
